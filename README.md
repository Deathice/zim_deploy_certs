Single-Node Commercial Certificate
=========

These roles will help you to deploy and install commercial certificates in remote nodes

Requirements
------------

Please, read the README.md file of each role, depending of your need.

License
-------

WTFPL

Author Information
------------------

* Work Mail: kperez@i-t-m.com
* Personal Mail: kevynkl2@gmail.com
* Cel: +(502) 5412-7538
