Single-Node Commercial Certificate
=========

This role will install a commercial certificate in a single zimbra node

Requirements
------------

* ansible 2.9+
* Access to the remote server with administrator user via ssh (e.g. root)
* If you don't have the ca bundle, put the following files into the files/ directory of your role:
    1. Certificate <-- must be called "cert.crt"
    2. CA root <-- must be called "ca_root.crt"
    3. Intermediate <-- must be called "intermediate.crt"

**NOTE: with the above files, the role automatically will create the ca bundle file**

* If you already have the ca bundle, only put the following files into the files/ directory of your role:
    1. Certificate <-- must be called "cert.crt"
    2. Ca Bundle <-- must be called "ca_chain.crt"

Role Variables
--------------

If you already have the ca bundle file, please change the default value of create_bundle variable of `True` to `False`

Example:

```
roles:
    - role: sn_comm_cert
      vars:
        create_bundle: false
```
    

Example Playbook
----------------

```
- name: Deploying certificate
  hosts: all

  roles:
    - role: sn_comm_cert
```

License
-------

WTFPL

Author Information
------------------

* Work Mail: kperez@i-t-m.com
* Personal Mail: kevynkl2@gmail.com
* Cel: +(502) 5412-7538